import kotlinx.serialization.Serializable

@kotlinx.serialization.Serializable
data class Shop(val name: String) {
    val id: Int = name.hashCode() //TODO: Replace with UUID or ID from DB

    companion object {
        const val path = "/shopList"
    }
}