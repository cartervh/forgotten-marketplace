import io.ktor.http.*
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer

import kotlinx.browser.window

val endpoint = window.location.origin // only needed until https://youtrack.jetbrains.com/issue/KTOR-453 is resolved

val jsonClient = HttpClient {
    install(JsonFeature) { serializer = KotlinxSerializer() }
}

suspend fun getShops(): List<Shop> {
    return jsonClient.get(endpoint + Shop.path)
}

suspend fun addShop(shop: Shop) {
    jsonClient.post<Unit>(endpoint + Shop.path) {
        contentType(ContentType.Application.Json)
        body = shop
    }
}

suspend fun deleteShop(shop: Shop) {
    jsonClient.delete<Unit>(endpoint + Shop.path + "/${shop.id}")
}