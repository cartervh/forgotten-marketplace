import react.*
import react.dom.*
import kotlinext.js.*
import kotlinx.html.js.*
import kotlinx.coroutines.*

private val scope = MainScope()

val app = fc<Props> { _ ->
    var shopList by useState(emptyList<Shop>())

    useEffectOnce {
        scope.launch {
            shopList = getShops()
        }
    }

    h1 {
        +"Forgotten Marketplace"
    }
    ul {
        shopList.sortedBy(Shop::name).forEach { shop ->
            li {
                key = shop.toString()
                +"${shop.name}"
                attrs {
                    onClick = {
                        scope.launch {
                            deleteShop(shop)
                            shopList = getShops()
                        }
                    }
                }
            }
        }
    }

    child(inputComponent) {
        attrs.onSubmit = { input ->
            val shop = Shop(input)
            scope.launch {
                addShop(shop)
                shopList = getShops()
            }
        }
    }
}